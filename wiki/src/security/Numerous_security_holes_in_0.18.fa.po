# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-19 06:45+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/numerous_"
"security_holes_in_018/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Sat Jun 22 00:00:00 2013\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 0.18\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid "Several security holes affect Tails 0.18."
msgstr ""

#. type: Plain text
msgid ""
"We **strongly** urge you to [[upgrade to Tails 0.19|news/version_0.19]] as "
"soon as possible in case you are still using an older version."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Details\n"
msgstr "جزییات\n"

#. type: Bullet: ' - '
msgid ""
"Iceweasel ( [MFSA "
"2013-49](https://www.mozilla.org/security/announce/2013/mfsa2013-49.html)  "
"[MFSA "
"2013-50](https://www.mozilla.org/security/announce/2013/mfsa2013-50.html), "
"[MFSA "
"2013-51](https://www.mozilla.org/security/announce/2013/mfsa2013-51.html), "
"[MFSA "
"2013-53](https://www.mozilla.org/security/announce/2013/mfsa2013-53.html), "
"[MFSA "
"2013-54](https://www.mozilla.org/security/announce/2013/mfsa2013-54.html), "
"[MFSA "
"2013-55](https://www.mozilla.org/security/announce/2013/mfsa2013-55.html), "
"[MFSA "
"2013-56](https://www.mozilla.org/security/announce/2013/mfsa2013-56.html), "
"[MFSA "
"2013-59](https://www.mozilla.org/security/announce/2013/mfsa2013-59.html), )"
msgstr ""

#. type: Bullet: ' - '
msgid "tiff ([[!debsa2013 2698]])"
msgstr ""

#. type: Bullet: ' - '
msgid "gnutls26 ([[!debsa2013 2697]])"
msgstr ""
