# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-12-15 16:51+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title ##
#, no-wrap
msgid "New features"
msgstr "Nouvelles fonctionnalités"

#. type: Bullet: '- '
msgid ""
"<span "
"class=\"application\">[[Icedove|doc/anonymous_internet/icedove]]</span> a "
"rebranded version of <span class=\"application\">Mozilla Thunderbird</span> "
"is now the official email client in Tails, replacing <span "
"class=\"application\">Claws Mail</span>."
msgstr ""
"<span class=\"application\">[[Icedove|doc/anonymous_internet/icedove]]</"
"span>, une version rebaptisée de <span class=\"application\">Mozilla "
"Thunderbird</span>, est maintenant le client mail officiel de Tails et "
"remplace <span class=\"application\">Claws Mail</span>."

#. type: Plain text
#, no-wrap
msgid ""
"  <span class=\"application\">Claws Mail</span> will be removed from Tails "
"in\n"
"  version 2.0 (2016-01-26). If you have been using\n"
"  <span class=\"application\">Claws Mail</span> and activated its "
"persistence\n"
"  feature, follow our instructions to [[migrate your data to\n"
"  <span "
"class=\"application\">Icedove</span>|doc/anonymous_internet/claws_mail_to_icedove]].\n"
msgstr ""
"  <span class=\"application\">Claws Mail</span> sera supprimé de Tails à la version\n"
"  2.0 (26-01-2016). Si vous utilisez\n"
"  <span class=\"application\">Claws Mail</span> et avez activé sa persistance,\n"
"  suivez nos instructions pour [[migrer vos données vers\n"
"  <span class=\"application\">Icedove</span>|doc/anonymous_internet/claws_mail_to_icedove]].\n"

#. type: Title ##
#, no-wrap
msgid "Upgrades and changes"
msgstr "Mises à jours et changements"

#. type: Bullet: '- '
msgid ""
"Electrum from 1.9.8 to "
"[2.5.4](https://github.com/spesmilo/electrum/blob/0d4de870a5159d491b604d29780f6bfb539c49f3/RELEASE-NOTES).  "
"Now Electrum should work again in Tails."
msgstr ""
"Electrum a été mis à jour de la version 1.9.8 vers la version [2.5.4]"
"(https://github.com/spesmilo/electrum/"
"blob/0d4de870a5159d491b604d29780f6bfb539c49f3/RELEASE-NOTES).  Electrum "
"devrait de nouveau fonctionner."

#. type: Plain text
msgid ""
"- Tor Browser to "
"[5.0.5](https://gitweb.torproject.org/builders/tor-browser-bundle.git/tree/Bundle-Data/Docs/ChangeLog.txt?h=maint-5.0&id=tbb-5.0.5-build1)."
msgstr ""
"- Tor Browser a été mis à jour vers la version [5.0.5](https://gitweb."
"torproject.org/builders/tor-browser-bundle.git/tree/Bundle-Data/Docs/"
"ChangeLog.txt?h=maint-5.0&id=tbb-5.0.5-build1)."

#. type: Plain text
msgid ""
"- Tor to "
"[0.2.7.6](https://gitweb.torproject.org/tor.git/tree/ChangeLog?id=tor-0.2.7.6)."
msgstr ""
"- Tor a été mis à jour vers la version [0.2.7.6](https://gitweb.torproject."
"org/tor.git/tree/ChangeLog?id=tor-0.2.7.6)."

#. type: Plain text
msgid ""
"- I2P to "
"[0.9.23](https://geti2p.net/en/blog/post/2015/11/19/0.9.23-Release)."
msgstr ""
"- I2P a été mis à jour vers la version [0.9.23](https://geti2p.net/en/blog/"
"post/2015/11/19/0.9.23-Release)."

#. type: Plain text
msgid "- Icedove from 31.8 to 38.4."
msgstr "- Icedove a été mis à jour de la version 31.8 à la version 38.4."

#. type: Plain text
msgid "- Enigmail from 1.7.2 to 1.8.2."
msgstr "- Enigmail a été mis à jour de la version 1.7.2 à la version 1.8.2."
