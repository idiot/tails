# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2015-09-22 18:35+0300\n"
"PO-Revision-Date: 2015-09-30 19:47+0100\n"
"Last-Translator: Tails translators <tails@boum.org>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#. type: Title ##
#, no-wrap
msgid "Upgrades and changes"
msgstr "Aktualisierungen und Änderungen"

#. type: Bullet: '  - '
msgid "Upgrade *Tor Browser* to version 5.0.3 (based on Firefox 38.3.0 ESR)."
msgstr ""
"*Tor Browser* wurde auf Version 5.0.3 (basierend auf Firefox 38.3.0 ESR) "
"aktualisiert."

#. type: Bullet: '  - '
msgid "Upgrade *I2P* to version 0.9.22 and enable its AppArmor profile."
msgstr ""
"*I2P* wurde auf  Version 0.9.22 aktualisiert und das AppArmor-Profil wurde "
"aktiviert"

#. type: Plain text
msgid ""
"There are numerous other changes that might not be apparent in the daily "
"operation of a typical user. Technical details of all the changes are listed "
"in the [Changelog](https://git-tails.immerda.ch/tails/plain/debian/"
"changelog)."
msgstr ""
"Es gibt viele weitere Änderungen, die im täglichen Betrieb eines typischen "
"Nutzenden nicht offensichtlich sind. Die technischen Details aller "
"Änderungen sind im [Changelog](https://git-tails.immerda.ch/tails/plain/"
"debian/changelog) aufgelistet."

#. type: Title ##
#, no-wrap
msgid "Fixed problems"
msgstr "Behobene Probleme"

#. type: Plain text
#, no-wrap
msgid ""
"  - Fix several issues related to *MAC address spoofing*:\n"
"    * If MAC address spoofing fails on a network interface and\n"
"      this interface cannot be disabled, then all networking is now\n"
"      completely disabled.\n"
"    * A notification is displayed if MAC address spoofing causes network\n"
"      issues, for example if a network only allows connections from a\n"
"      list of authorized MAC addresses.\n"
msgstr ""
"  - Mehrere Fehler im Bezug auf das *Verschleiern von MAC-Adressen* wurden behoben:\n"
"    * Falls das Verschleiern der MAC-Adresse von einer Netzwerkschnittstelle\n"
"      fehlschlägt und die Schnittstelle nicht deaktiviert werden kann,\n"
"      werden die Netzwerkfunktionen vollständig deaktiviert\n"
"    * Es wird eine Benachrichtigung angezeigt, wenn das Verschleiern der MAC-Adresse\n"
"      zu Netzwerkproblemen führt, beispielweise wenn das Netzwerk nur Verbindungen\n"
"      von einer Liste von autorisierten MAC-Adressen zulässt.\n"
