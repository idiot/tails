# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-02-07 13:34+0100\n"
"PO-Revision-Date: 2014-01-29 16:55-0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Wed Feb 5 10:00:00 2014\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 0.22.1 is out\"]]\n"
msgstr "[[!meta title=\"Tails 0.22.1 est sorti\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr ""

#. type: Plain text
msgid "Tails, The Amnesic Incognito Live System, version 0.22.1, is out."
msgstr "Tails, The Amnesic Incognito Live System, version 0.22.1 est sorti."

#. type: Plain text
msgid ""
"All users must upgrade as soon as possible: this release fixes [[numerous "
"security issues|security/Numerous_security_holes_in_0.22]]."
msgstr ""
"Tous les utilisateurs doivent le mettre à jour dès que possible : cette "
"version corrige de [[nombreux problèmes de sécurité|security/"
"Numerous_security_holes_in_0.22]]."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Changes"
msgstr "Changements"

#. type: Plain text
msgid "Notable user-visible changes include:"
msgstr "Les changements notables visibles pour l'utilisateur comprennent :"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "* Security fixes\n"
#| "  - Upgrade Iceweasel to 24.3.0esr, that fixes a few serious\n"
#| "    security issues.\n"
#| "  - Upgrade the system NSS to 3.14.5, that fixes a few serious\n"
#| "    security issues.\n"
#| "  - Workaround a browser size fingerprinting issue by using small\n"
#| "    icons in the web browser's navigation toolbar.\n"
msgid ""
"* Security fixes\n"
"  - Upgrade the web browser to 24.3.0esr, that fixes a few serious\n"
"    security issues.\n"
"  - Upgrade the system NSS to 3.14.5, that fixes a few serious\n"
"    security issues.\n"
"  - Workaround a browser size fingerprinting issue by using small\n"
"    icons in the web browser's navigation toolbar.\n"
"  - Upgrade Pidgin to 2.10.8, that fixes a number of serious\n"
"    security issues.\n"
msgstr ""
"* Sécurité\n"
" - Mise à jour d' Iceweasel vers la version 24.3.0esr, corrigeant quelques sérieux\n"
"    problèmes de sécurité.\n"
"  - Mise à jour du système NSS vers la version 3.14.5, corrigeant quelques sérieux\n"
"    problèmes de sécurité.\n"
"  - Contournement d'un problème d'empreinte de la taille du navigateur en utilisant\n"
"    de petits icônes dans la barre de navigation du navigateur web.\n"

#. type: Plain text
#, no-wrap
msgid ""
"* Major improvements\n"
"  - Check for upgrades availability using Tails Upgrader, and propose\n"
"    to apply an incremental upgrade whenever possible.\n"
"  - Install Linux 3.12 (3.12.6-2).\n"
msgstr ""
"* Améliorations majeures\n"
"  - Vérification de la disponibilité de mises à jour via le Tails Upgrader, et\n"
"    proposition de mises à jour incrémentales quand cela est possible.\n"
"  - Installation de Linux 3.12 (3.12.6-2).\n"

#. type: Plain text
#, no-wrap
msgid ""
"* Bugfixes\n"
"  - Fix the keybindings problem introduced in 0.22.\n"
"  - Fix the Unsafe Browser problem introduced in 0.22.\n"
"  - Use IE's icon in Windows camouflage mode.\n"
"  - Handle some corner cases better in Tails Installer.\n"
"  - Use the correct browser homepage in Spanish locales.\n"
msgstr ""
"* Corrections de bugs\n"
"  - Correction du problème de raccourcis clavier introduit dans la 0.22.\n"
"  - Correction du problème de Navigateur Non-sécurisé introduit dans la 0.22.\n"
"  - Utilisation de l'icône d'IE en mode camouflage Windows.\n"
"  - Meilleure prise en charge de cas limites dans l'Installeur de Tails.\n"
"  - Utilisation de la bonne page d'accueil du navigateur en espagnol.\n"

#. type: Plain text
#, no-wrap
msgid ""
"* Minor improvements\n"
"  - Update Torbutton to 1.6.5.3.\n"
"  - Do not start Tor Browser automatically, but notify when Tor\n"
"    is ready.\n"
"  - Import latest Tor Browser prefs.\n"
"  - Many user interface improvements in Tails Upgrader.\n"
msgstr ""
"* Améliorations mineures\n"
"  - Mise à jour du Torbutton vers la version 1.6.5.3.\n"
"  - Le navigateur web n'est pas lancé automatiquement, mais\n"
"     une notification prévient quand Tor est prêt.\n"
"  - Importation des dernières préférences du Tor Browser.\n"
"  - Nombreuses améliorations de l'interface utilisateur du Tails Upgrader.\n"

#. type: Plain text
msgid ""
"See the [online Changelog](https://git-tails.immerda.ch/tails/plain/debian/"
"changelog)  for technical details."
msgstr ""
"Voir le [journal des modifications](https://git-tails.immerda.ch/tails/plain/"
"debian/changelog) pour les détails techniques."

#. type: Title #
#, no-wrap
msgid "Known issues"
msgstr "Problèmes connus"

#. type: Bullet: '* '
msgid ""
"The memory erasure on shutdown [[!tails_ticket 6460 desc=\"does not work on "
"some hardware\"]]."
msgstr ""
"L'effacement de la mémoire à l'extinction [[!tails_ticket 6460 desc=\"ne "
"fonctionne pas sur certains matériels\"]]."

#. type: Bullet: '* '
msgid "[[Longstanding|support/known_issues]] known issues."
msgstr "Problèmes connus de [[longue date|support/known_issues]]."

#. type: Title #
#, no-wrap
msgid "I want to try it or to upgrade!"
msgstr "Je veux l'essayer ou le mettre à jour !"

#. type: Plain text
msgid "Go to the [[download]] page."
msgstr ""

#. type: Plain text
msgid ""
"As no software is ever perfect, we maintain a list of [[problems that "
"affects the last release of Tails|support/known_issues]]."
msgstr ""
"Comme aucun logiciel n'est parfait, nous maintenons une liste des "
"[[problèmes qui affectent la dernière version de Tails|support/"
"known_issues]]."

#. type: Title #
#, no-wrap
msgid "What's coming up?"
msgstr "Et après ?"

#. type: Plain text
msgid ""
"The next Tails release is [[scheduled|contribute/calendar]] for March 18."
msgstr ""
"La prochaine version de Tails est [[prévue|contribute/calendar]] pour le 18 "
"mars."

#. type: Plain text
msgid "Have a look to our [[!tails_roadmap]] to see where we are heading to."
msgstr ""
"Jetez un œil à notre [[!tails_roadmap desc=\"feuille de route\"]] pour voir "
"vers où nous allons."

#. type: Plain text
msgid ""
"Would you want to help? There are many ways [[**you** can contribute to "
"Tails|contribute]]. If you want to help, come talk to us!"
msgstr ""
"Vous voulez aider ? Il y a de nombreuses façons pour [[**vous** de "
"contribuer|contribute]]. Si vous souhaitez aider, venez nous en parler !"

#~ msgid ""
#~ "Go to the [[download]] page but first, please consider [[testing the "
#~ "incremental upgrade|news/test_incremental_upgrades]]."
#~ msgstr ""
#~ "Aller à la page de [[téléchargement|download]] mais tout d'abord, "
#~ "veuillez tester les [[mises à jour incrémentales|upgrade|news/"
#~ "test_incremental_upgrades]]."
