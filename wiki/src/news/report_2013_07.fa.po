# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-22 09:20+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian "
"<http://weblate.451f.org:8889/projects/tails/report_2013_07/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Thu Aug 13 09:09:39 2013\"]]\n"
msgstr "[[!meta date=\"پنج‌شنبه ۱۳ آگوست ۰۹:۰۹:۳۹ ۲۰۱۳\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails report for July, 2013\"]]\n"
msgstr "[[!meta title=\"گزارش ژوییه ۲۰۱۳ تیلز\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title =
#, no-wrap
msgid "Tails summit\n"
msgstr "نشست تیلز\n"

#. type: Plain text
msgid ""
"Five developers met for our annual summit. The summit was less dense than "
"last year but still very productive. See the [[detailed "
"report|news/summit_2013]]."
msgstr ""
"پنج توسعه‌گر تیلز برای نشست سالانه دور هم جمع شدند. این نشست فشردگی کمتری "
"نسبت به سال گذشته داشت اما همچنان بسیار مفید بود. رجوع کنید به [[گزارش "
"مشروح|news/summit_2013]]."

#. type: Title =
#, no-wrap
msgid "Releases\n"
msgstr "انتشارها\n"

#. type: Plain text
msgid ""
"There was no release in July. Tails 0.20 [is "
"planned](https://mailman.boum.org/pipermail/tails-dev/2013-July/003292.html)  "
"for the 9th of August."
msgstr ""
"در ماه ژوییه انتشاری نداشتیم. تیلز ۰٫۲۰ [قرار "
"است](https://mailman.boum.org/pipermail/tails-dev/2013-July/003292.html) روز "
"۹ آگوست منتشر شود."

#. type: Title =
#, no-wrap
msgid "Metrics\n"
msgstr "متریک‌ها\n"

#. type: Bullet: '- '
msgid ""
"151 657 connections of Tails to the Tor network. This makes a boot every "
"17.7 seconds on average. This is an approximation from the requests made to "
"the security announcements feed when Tails is connected to Tor."
msgstr ""
"۱۵۱٫۶۵۷ اتصال از تیلز به شبکهٔ تور ایجاد شد، یعنی هر ۱۷٫۷ ثانیه یک راه‌"
"اندازی. این عدد بر حسب درخواست‌ها برای خوراک اعلان‌های امنیتی پس از متصل شدن "
"تیلز به تور محاسبه می‌شود."

#. type: Plain text
msgid "- 77 non-empty bug reports were received through WhisperBack."
msgstr "۷۷ گزارش ایراد غیرخالی ارسال‌شده از ویسپربک دریافت کردیم."

#. type: Title =
#, no-wrap
msgid "Code\n"
msgstr "کد\n"

#. type: Bullet: '- '
msgid ""
"We had two low-hanging fruits sessions, that were mostly used to review and "
"merge pending branches."
msgstr ""
"دو نشست رسیدگی به کارهای سادهٔ در دسترس داشتیم که در آن‌ها به بررسی و ادغام "
"شاخه‌های نیمه‌کاره پرداختیم."

#. type: Bullet: '- '
msgid ""
"A lot of work has been done to [[!tails_gitweb_branch feature/wheezy "
"desc=\"migrate to Debian Wheezy\"]]:"
msgstr ""
"کارهای زیادی برای [[!tails_gitweb_branch feature/wheezy desc=\"کوچ به Debian "
"Wheezy\"]] انجام دادیم:"

#. type: Bullet: '- '
msgid ""
"A first build of Tails based on Debian Wheezy version was tested, and many "
"bugs [[!tails_ticket 6015 desc=\"were identified\"]]."
msgstr ""
"اولین ساخت تیلز مبتنی بر Debian Wheezy آزمایش شد و [[!tails_ticket 6015 desc="
"\"ایرادهای بسیاری\"]] در آن یافت شد."

#. type: Bullet: '- '
msgid ""
"The work on AppArmor was [[!tails_gitweb_branch feature/apparmor "
"desc=\"continued\"]]. An early upstream patch that aims to add support for "
"stacked filesystems [[!tails_ticket 6199 desc=\"was tested\"]]."
msgstr ""
"کار روی اپ‌آرمور [[!tails_gitweb_branch feature/apparmor desc=\"ادامه یافت\""
"]]. یک بستهٔ زودهنگام آپ‌استریم با هدف افزایش پشتیبانی فایل‌های سیستمی ذخیره‌"
"شده [[!tails_ticket 6199 desc=\"امتحان شد\"]]."

#. type: Bullet: '- '
msgid ""
"The included Linux kernel was [[!tails_gitweb_branch feature/linux-3.10 "
"desc=\"updated\"]] to 3.10-1."
msgstr ""
"هستهٔ گنجانده‌شدهٔ لینوکس به ۳٫۱۰-۱ ails_gitweb_branch feature/linux-3.10 "
"desc=\"ارتقاء داده شد\"]]."

#. type: Bullet: '- '
msgid ""
"A deprecation wrapper for Truecrypt was [[!tails_gitweb_branch "
"feature/truecrypt_deprecation_wrapper desc=\"written\"]]. Many thanks to "
"Julien Voisin for providing patches."
msgstr ""
"یک deprecation wrapper برای تروکریپت [[!tails_gitweb_branch feature/"
"truecrypt_deprecation_wrapper desc=\"نوشته شد\"]]. تشکر بسیار از جولین "
"وویسین به خاطر آماده کردن بسته‌ها."

#. type: Bullet: '- '
msgid ""
"Dasher, a graphical predictive text input system, was [[!tails_gitweb_branch "
"feature/install-dasher desc=\"installed\"]]."
msgstr ""
"Dasher، یک سیستم گرافیک پیش‌بینی‌کنندهٔ ورودی متن [[!tails_gitweb_branch "
"feature/install-dasher desc=\"نصب شد\"]]."

#. type: Bullet: '- '
msgid ""
"The Wikileaks IRC server address was [[!tails_gitweb_branch "
"bugfix/wikileaks_irc desc=\"tentatively fixed\"]]. Many thanks to Julien "
"Voisin for providing patches. User interface issues have prevented us from "
"merging this branch yet."
msgstr ""
"نشانی سرور IRC ویکی‌لیکس [[!tails_gitweb_branch bugfix/wikileaks_irc desc=\""
"به طور آزمایشی ترمیم شد\"]]. تشکر بسیار از جولین وویسین برای آماده کردن بسته‌"
"ها. مشکلات رابط کاربری هنوز امکان ادغام بسته‌ها را به ما نداده است."

#. type: Bullet: '- '
msgid ""
"Claws mail will [[!tails_gitweb_commit "
"7a293bbc41766c4ba32e229acb862eabe05d05ce desc=\"no more display a dialog "
"box\"]] when sending a message. Many thanks to matsa for providing a patch."
msgstr ""
"نرم‌افزار Claws Mail دیگر هنگام ارسال یک رایانامه [[!tails_gitweb_commit "
"7a293bbc41766c4ba32e229acb862eabe05d05ce desc=\"دیگر پنجرهٔ گفتگو نشان نمی‌"
"دهد\"]]. تشکر بسیار از matsa برای ایجاد بسته."

#. type: Bullet: '- '
msgid ""
"Many [[!tails_gitweb_branch feature/remember_installed_packages "
"desc=\"improvements\"]] to additional software have been merged."
msgstr ""
"[[!tails_gitweb_branch feature/remember_installed_packages desc=\"پیشرفت‌"
"های\"]] بسیاری در نرم‌افزارهای اضافی ادغام شده‌اند."

#. type: Bullet: '- '
msgid ""
"A bunch of improvements to our test suite were proposed and reviewed: "
"[[!tails_gitweb_branch test/fix-persistence-checks]], [[!tails_gitweb_branch "
"test/reorg]], [[!tails_gitweb_branch test/firewall-check-tag]], "
"[[!tails_gitweb_branch test/keep-volumes-tag]] and [[!tails_gitweb_branch "
"test/fix-iso-reporting]]."
msgstr ""
"پیشرفت‌هایی برای بستهٔ آزمایشی ما پیشنهاد و بررسی شدند: [["
"!tails_gitweb_branch test/fix-persistence-checks]]، [[!tails_gitweb_branch "
"test/reorg]], [[!tails_gitweb_branch test/firewall-check-tag]]، [["
"!tails_gitweb_branch test/keep-volumes-tag]] و [[!tails_gitweb_branch test"
"/fix-iso-reporting]]."

#. type: Title =
#, no-wrap
msgid "Documentation and Website\n"
msgstr "مستندسازی و تارنما\n"

#. type: Bullet: '- '
msgid ""
"The [[Promote|contribute/how/promote]] page was [[!tails_gitweb_commit "
"c8f80ebb18a6077feacbf00e7c31a19a99b22648 desc=\"fully rewritten\"]]."
msgstr ""
"صفحهٔ [[تبلیغ|contribute/how/promote]] [[!tails_gitweb_commit "
"c8f80ebb18a6077feacbf00e7c31a19a99b22648 desc=\"کامل بازنویسی شد\"]]."

#. type: Bullet: '- '
msgid ""
"A new page [[Website|contribute/how/website]] on how to improve the Tails "
"website was [[!tails_gitweb_commit fb9135c3ba1cbb974f9ab3bf596d17ce03803e4a "
"desc=\"added to the contribute section\"]]."
msgstr ""
"صفحهٔ جدید [[تارنما|contribute/how/website]] در مورد چگونگی بهبود تارنمای "
"تیلز [[!tails_gitweb_commit fb9135c3ba1cbb974f9ab3bf596d17ce03803e4a desc=\""
"به قسمت همکاری اضافه شد\"]]."

#. type: Bullet: '- '
msgid ""
"The page [[Translate|contribute/how/translate]] was [[!tails_gitweb_branch "
"doc/either_git_or_transifex desc=\"reworked in depth\"]] to match our new "
"translation workflow using either Git or Transifex."
msgstr ""
"صفحهٔ [[ترجمه|contribute/how/translate]][[!tails_gitweb_branch doc/"
"either_git_or_transifex desc=\"تغییرات بسیاری یافت\"]] تا مناسب سیستم کاری "
"جدید ما برای ترجمه شود چه در گیت و چه در Transifex."

#. type: Bullet: '- '
msgid ""
"The [[Pidgin|doc/anonymous_internet/pidgin]] documentation was "
"[[!tails_gitweb_branch doc/better-pidgin-and-otr-documentation desc=\"was "
"fully rewritten\"]]."
msgstr ""
"مستندات [[پیجین|doc/anonymous_internet/pidgin]] [[!tails_gitweb_branch doc"
"/better-pidgin-and-otr-documentation desc=\"کامل بازنویسی شد\"]]."

#. type: Bullet: '- '
msgid ""
"The [[homepage|index]] was [[!tails_gitweb_branch doc/circumvention "
"desc=\"rephrased\"]] to mention Internet circumvention."
msgstr ""
"صفحهٔ [[خانه|index]] [[!tails_gitweb_branch doc/circumvention desc=\"تغییر "
"یافت\"]] تا به فیلترینگ اینترنت اشاره کند."

#. type: Bullet: '- '
msgid ""
"Our Tor enforcement policy was [[!tails_gitweb_commit "
"e0dba123f20e0df72ac723510b6fba093a1a29d7 desc=\"clarified\"]] on the "
"[[About|about]] page."
msgstr ""
"به سیاست اجبار استفاده از تور در صفحهٔ [[درباره|about]] [["
"!tails_gitweb_commit e0dba123f20e0df72ac723510b6fba093a1a29d7 desc=\"به وضوح "
"اشاره شد\"]]."

#. type: Bullet: '- '
msgid ""
"The OpenPGP signature to verify the ISO images is now [[!tails_gitweb_branch "
"doc/fix_signature_link desc=\"served directly by our website\"]]."
msgstr ""
"امضای اُپن‌پی‌جی‌پی برای تأیید تصویر ایزو حالا [[!tails_gitweb_branch doc/"
"fix_signature_link desc=\"مستقیم روی تارنمای خود ما قرار دارند\"]]."

#. type: Bullet: '- '
msgid ""
"A prototype conversion of our test suite to use [[!tails_gitweb_branch "
"test/rjb-migration desc=\"native ruby + rjb instead of JRuby\"]] was pushed "
"to Git."
msgstr ""
"یک تبدیل نمونه از بستهٔ آزمایش ما برای استفاده از [[!tails_gitweb_branch test"
"/rjb-migration desc=\"ruby + rjb بومی به جای JRuby\"] به گیت برده شد."

#. type: Title =
#, no-wrap
msgid "Localization and Internationalization\n"
msgstr "بومی‌سازی و بین‌المللی‌سازی\n"

#. type: Plain text
msgid ""
"- All Tails strings but the website are now translatable in Transifex.  - "
"i18nspector was uploaded to wheezy-backports."
msgstr ""
"- تمام رشته‌های تیلز به جز تارنما را حالا می‌توان از Transifex ترجمه کرد.  - "
"i18nspector به wheezy-backports آپلود شد."

#. type: Title =
#, no-wrap
msgid "Infrastructure\n"
msgstr "زیرساخت\n"

#. type: Plain text
msgid "- Our todo list items were [[!tails_redmine desc=\"migrated to Redmine\"]]."
msgstr ""
"- آیتم‌های فهرست کارهای پیش رویمان به [[!tails_redmine desc=\"ردماین برده "
"شد\"]]."

#. type: Bullet: '- '
msgid ""
"Blueprints were extracted from our old todo section into the new "
"[[Blueprint|blueprint]] section to store our research and plans in a static "
"form outside of Redmine."
msgstr ""
"بلوپرینت‌هایی از بخش قدیمی کارهای پیش رو به بخش جدید "
"[[بلوپرینت‌ها|blueprint]] برده شدند تا بررسی‌ها و برنامه‌های ما را به شکلی "
"ایستا خارج از ردماین ذخیره کنند."

#. type: Bullet: '- '
msgid ""
"Criterion to flag a task as easy were "
"[[identified|contribute/working_together/criteria_for_easy_tasks]] as a way "
"to orientate better new contributors."
msgstr ""
"معیارهای برچسب «آسان» زدن به یک کار [[تعریف "
"شدند|contribute/working_together/criteria_for_easy_tasks]] تا مشارکت‌کنندگان "
"جدید بهتر راهنمایی شوند."

#. type: Bullet: '- '
msgid ""
"Our roadmap was updated and we defined our objectives for Tails 1.0, 1.1, "
"2.0, and 3.0:"
msgstr ""
"نقشهٔ راه ما به‌روز شد و اهدافمان برای تیلز ۱٫۰، ۱٫۱،  ۲٫۰ و ۳٫۰ را تعریف "
"کردیم:"

#. type: Bullet: '  - '
msgid ""
"[Tails 1.0](https://labs.riseup.net/code/projects/tails/roadmap#Tails 1.0): "
"feature parity with Incognito"
msgstr ""
"[تیلز ۱٫۰](https://labs.riseup.net/code/projects/tails/roadmap#Tails 1.0): "
"تشابه ویژگی‌ها با Incognito"

#. type: Bullet: '  - '
msgid ""
"[Tails 1.1](https://labs.riseup.net/code/projects/tails/roadmap#Tails 1.1): "
"Tails based on Debian Wheezy"
msgstr ""
"[تیلز ۱٫۱](https://labs.riseup.net/code/projects/tails/roadmap#Tails 1.1): "
"تیلز مبتنی بر Debian Wheezy"

#. type: Bullet: '  - '
msgid ""
"[Tails 2.0](https://labs.riseup.net/code/projects/tails/roadmap#Tails 2.0): "
"Sustainability and maintainability"
msgstr ""
"[تیلز ۲٫۰](https://labs.riseup.net/code/projects/tails/roadmap#Tails 2.0): "
"پایداری و نگهداری‌پذیری"

#. type: Bullet: '  - '
msgid ""
"[Tails 3.0](https://labs.riseup.net/code/projects/tails/roadmap#Tails 3.0): "
"Hardening and better security"
msgstr ""
"[تیلز ۳٫۰](https://labs.riseup.net/code/projects/tails/roadmap#Tails 3.0): "
"امنیت بیشتر"

#. type: Bullet: '- '
msgid ""
"We started working on making our infrastructure more reliable: "
"[[!tails_ticket 6185 desc=\"looking for a system\"]] that could act as a "
"fail-over for our main server."
msgstr ""
"شروع به کار روی قابل‌اتکاءتر کردن زیرساخت خود کرده‌ایم: [[!tails_ticket 6185 "
"desc=\"گشتن به دنبال سیستمی\"]] که بتواند یک fail-over برای سرور اصلی‌مان "
"باشد."

#. type: Title =
#, no-wrap
msgid "Funding\n"
msgstr "جذب سرمایه\n"

#. type: Bullet: '- '
msgid ""
"A short-term funding with sponsor Bravo has been tentatively confirmed and "
"redefined, but not signed yet."
msgstr ""
"برای جذب سرمایهٔ کوتاه‌مدت موقتاً با اسپانسر براوو به توافق رسیده‌ایم و آن "
"را بازتعریف کرده‌ایم، اما توافق هنوز امضاء نشده‌است."

#. type: Bullet: '- '
msgid ""
"The contracts for the bounty program are still being signed. But some work "
"has already been done and tested on Seahorse Nautilus, AppArmor, ikiwiki to "
"Redmine, keyringer and libvirt."
msgstr ""
"امضای قراردادهای برنامه جایزه‌بگیری هنوز ادامه دارد. اما تا همین حالا "
"کارهایی روی Seahorse Nautilus، اپ‌آرمور، ikiwiki، ردماین، کی‌رینگر و libvirt "
"انجام و آزمایش شده‌اند."

#. type: Bullet: '- '
msgid "We sent a concept note to answer sponsor Charlie's call for proposals."
msgstr ""
"توضیحی در مورد خود برای پاسخ به درخواست پیشنهادهای اسپانسر چارلی فرستادیم."
