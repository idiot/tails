# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2016-01-14 14:46+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title =
#, no-wrap
msgid "Open these instructions on another device\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">[[!img install/inc/infography/switch-context.png link=\"no\"]]</div>\n"
msgstr ""

#. type: Plain text
msgid ""
"In the next step, you will shut down the computer. To be able to follow the "
"rest of the instructions afterwards, we recommend you either:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/qrcodes/tails_boum_org_install_clone.png class=\"install-clone qrcode\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/qrcodes/tails_boum_org_install_win_usb.png class=\"windows qrcode\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/qrcodes/tails_boum_org_install_mac_usb.png class=\"mac-usb qrcode\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/qrcodes/tails_boum_org_install_mac_clone.png class=\"mac-clone qrcode\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/qrcodes/tails_boum_org_install_mac_dvd.png class=\"mac-dvd qrcode\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/qrcodes/tails_boum_org_install_debian_usb.png class=\"debian qrcode\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/qrcodes/tails_boum_org_install_expert_usb.png class=\"expert qrcode\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/qrcodes/tails_boum_org_install_linux_usb.png class=\"linux qrcode\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/qrcodes/tails_boum_org_upgrade_clone.png class=\"upgrade-clone qrcode\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/qrcodes/tails_boum_org_upgrade_tails.png class=\"upgrade-tails qrcode\" link=\"no\"]]\n"
msgstr ""

#. type: Bullet: '   - '
msgid ""
"Open this page on your smartphone, tablet, or another computer (recommended)."
msgstr ""

#. type: Bullet: '   - '
msgid "Print the rest of the instructions on paper."
msgstr ""

#. type: Bullet: '   - '
msgid "Take note of the URL of this page to be able to come back later:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"     <span class=\"install-clone\">`https://tails.boum.org/install/clone/`</span>\n"
"     <span class=\"windows\">`https://tails.boum.org/install/win/usb/`</span>\n"
"     <span class=\"mac-usb\">`https://tails.boum.org/install/mac/usb/`</span>\n"
"     <span class=\"mac-clone\">`https://tails.boum.org/install/mac/clone/`</span>\n"
"     <span class=\"mac-dvd\">`https://tails.boum.org/install/mac/dvd/`</span>\n"
"     <span class=\"debian\">`https://tails.boum.org/install/debian/usb/`</span>\n"
"     <span class=\"expert\">`https://tails.boum.org/install/expert/usb/`</span>\n"
"     <span class=\"linux\">`https://tails.boum.org/install/linux/usb/`</span>\n"
"     <span class=\"upgrade-clone\">`https://tails.boum.org/upgrade/clone/`</span>\n"
"     <span class=\"upgrade-tails\">`https://tails.boum.org/upgrade/tails/`</span>\n"
msgstr ""
